import { 
    StyleSheet,
    Dimensions,  
} from 'react-native';

const width = Dimensions.get('window').width;

export default StyleSheet.create({
    container: {
        width,
        height: 28,
        position: 'absolute',
        backgroundColor: '#FF0000',
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: 'black',
    }
});