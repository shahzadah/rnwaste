import React, { Component } from 'react';
import {
    View,
    Text,
    NetInfo,
} from 'react-native';
import styles from './styles';
import { strings } from '../../configs/i18n';

export default class OfflineStrip extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isNetworkAvailable: true
        };
    }

    render() {
        if (this.state.isNetworkAvailable) {
            return null;
        } else {
            return (
                < View style={styles.container}>
                    <Text style={styles.text}>{strings('no_internet_connection')}</Text>
                </View >
            );
        }
    }

    handleConnectivityChange = isConnected => {
        this.setState({
            isNetworkAvailable : isConnected,
        });
    }

    componentDidMount() {
        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
    }

    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
    }
}