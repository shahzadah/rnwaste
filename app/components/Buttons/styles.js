import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../configs/colors';

export default StyleSheet.create({
    background: {
        width: Dimensions.get('window').width * 0.8, 
        paddingTop: 10,
        paddingBottom: 10,
        borderWidth: 2,
        borderColor: colors.primaryText,
        borderRadius: 30,
    }, text: {
        textAlign: 'center',
        color: colors.primaryText,
        fontSize: 16,
    },
});