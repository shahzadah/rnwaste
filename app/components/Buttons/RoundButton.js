import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TouchableNativeFeedback,
} from 'react-native';
import colors from '../../configs/colors';
import styles from './styles.js';
import PropTypes from 'prop-types';

class RoundButton extends Component {

    render() {
        const { text, style, onPress } = this.props;
        return (
            <TouchableOpacity
                style={[styles.background, style]}
                onPress={() => onPress()}>
                <Text style={styles.text}>{text}</Text>
            </TouchableOpacity >
        );
    }
}

RoundButton.propTypes = {
    text: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    style: PropTypes.any,
};

export default RoundButton;