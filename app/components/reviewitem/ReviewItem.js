import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableNativeFeedback,
} from 'react-native';
import styles from './styles';
import colors from '../../configs/colors';

export default class ReviewItem extends Component {

    render() {
        const item = this.props.data;
        return (
            <TouchableNativeFeedback background={TouchableNativeFeedback.SelectableBackground()}>
                <View style={styles.container}>
                    <Image style={{ width: 100, height: 100 }} source={{ uri: item.picture.thumbnail }} />
                    <View style={styles.containerDetails}>
                        <Text style={styles.name}>{item.name.first} {item.name.last}</Text>
                        <Text style={styles.email}>{item.phone}</Text>
                        <Text style={styles.email}>{item.email}</Text>
                    </View>
                </View>
            </TouchableNativeFeedback>
        );
    }
}