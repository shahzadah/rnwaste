import {
    StyleSheet,
} from 'react-native';
import colors from '../../configs/colors';

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: colors.review_item_background,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 15,
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
    containerDetails: {
        flexDirection: 'column',
        marginLeft: 10,
    },
    image: {
        width: 100,
        height: 100,
    },
    name: {
        fontSize: 18,
        color: colors.grey_one,
    },
    email: {
        fontSize: 14,
        color: colors.grey_two,
    }
});