import React, { Component } from 'react';
import {
    View,
    Text,
    NetInfo,
    Alert,
    ActivityIndicator,
    StatusBar,
} from 'react-native';
import styles from '../configs/styles.js';
import { strings } from '../configs/i18n';
import colors from '../configs/colors';
import RoundButton from '../components/Buttons/RoundButton';
import OfflineStrip from '../components/OfflineSign/OfflineStrip';

export default class Landing extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: false,
        }
    }

    render() {
        const { isLoading } = this.state;
        return (
            <View style={styles.background}>
                <OfflineStrip />
                <StatusBar backgroundColor={colors.background} />
                <View style={styles.center}>
                    <Text
                        style={styles.appName}>
                        {strings('waste_mgmt_colleague_shop')}
                    </Text>

                    <RoundButton
                        text={strings('record_waste_surplus')}
                        onPress={() => { alert('waste surplus clicked') }}
                        style={{ marginTop: 50 }} />

                    <RoundButton
                        text={strings('review_colleague_shop')}
                        onPress={this.onReviewButtonClicked}
                        style={{ marginTop: 24 }} />

                    {isLoading ? <ActivityIndicator style={{ position: 'absolute' }} size="large" color={colors.progressBar} /> : null}
                </View>

            </View>
        );
    }

    onReviewButtonClicked = () => {
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected) {
                this.setState({ isLoading: true })
                this.callReviewAPI();
            } else {
                Alert.alert(
                    strings('no_internet_connection'),
                    strings('no_internet_connection_message')
                );
            }
        });
    }

    callReviewAPI = () => {
        const url = 'https://randomuser.me/api/?page=1&results=20';
        console.log('url', url);
        fetch(url)
            .then((response) => {
                this.setState({ isLoading: false });
                const statusCode = response.status;
                const data = response.json();
                console.log("status code: " + statusCode);
                console.log("data: " + data);
                return data;
            })
            .then((responseJson) => {
                console.log(responseJson["results"]);
                this.setState({ isLoading: false });
                this.props.navigation.navigate('Review', {
                    pageNo: 1,
                    resultArray: responseJson["results"]
                });
            })
            .catch((error) => {
                console.log('error', error);
                this.setState({ isLoading: false });
            });
    }
}