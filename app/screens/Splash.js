import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StatusBar,
} from 'react-native';
import styles from '../configs/styles';
import { strings } from '../configs/i18n';
import { version } from '../../package.json';
import colors from '../configs/colors';
import OfflineStrip from '../components/OfflineSign/OfflineStrip';

export default class Splash extends Component {

    render() {
        return (
            <View style={styles.background}>
                <OfflineStrip />
                <StatusBar backgroundColor={colors.background} />
                <View style={styles.center}>
                    <Image
                        style={{ resizeMode: Image.resizeMode.contain, }}
                        source={require("@images/logo_tesco.png")} />

                    <Text
                        style={styles.appName}>
                        {strings('app_name')}
                    </Text>
                </View>
                <Text
                    style={styles.version}>
                    {strings('version', { pkg_version: version })}
                </Text>
            </View>
        );
    }

    componentDidMount() {
        setTimeout(() => {
            this.props.navigation.replace('Landing');
        }, 1000);
    }
}