import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    StatusBar,
    Text,
    NetInfo,
    ActivityIndicator,
} from 'react-native';
import colors from '../configs/colors';
import ReviewItem from '../components/reviewitem/ReviewItem';

export default class Review extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isRefresh: false,
            isLoading: false,
            page: props.navigation.getParam('pageNo', 0),
            data: props.navigation.getParam('resultArray', []),
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={colors.background} />
                <FlatList
                    data={this.state.data}
                    renderItem={({ item }) => (
                        <ReviewItem data={item} />
                    )}
                    keyExtractor={item => item.email}
                    onEndReached={this.onLoadMore}
                    onEndReachedThreshold={3}
                    ListFooterComponent={this.renderFooter}
                    onRefresh={this.onRefresh}
                    refreshing={this.state.isRefresh} />
            </View>
        );
    }

    renderFooter = () => {
        if (!this.state.isLoading) {
            return null;
        }
        return (
            <View style={styles.footer}>
                <ActivityIndicator size="large" color={colors.progressBar} />
            </View>
        );
    }

    onRefresh = () => {
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected) {
                this.setState({ isRefresh: true, page: 1 });
                this.makeAPIRequest();
            }
        });
    }

    onLoadMore = () => {
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected) {
                this.setState({ isLoading: true, page: this.state.page + 1 });
                this.makeAPIRequest();
            }
        });
    }

    makeAPIRequest = () => {
        var pageNo = this.state.page;
        const url = `https://randomuser.me/api/?page=${pageNo}&results=20`;
        console.log('url', url);
        fetch(url)
            .then(response => {
                console.log('Status code', response.status);
                return response.json();
            })
            .then(responseJson => {
                console.log('response', responseJson["results"]);
                this.setState({
                    isRefresh: false,
                    isLoading: false,
                    data: [...this.state.data, ...responseJson["results"]]
                });
            })
            .catch(error => {
                console.log('error', error);
                this.setState({ isLoading: false, isRefresh: false });
            });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.background,
    },
    footer: {
        backgroundColor: colors.review_item_background,
        padding: 5,
    }
});