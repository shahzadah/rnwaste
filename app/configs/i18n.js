import ReactNative from 'react-native';
import I18n from 'react-native-i18n';

import en from '../../locales/en';
import ar from '../../locales/ar';

//Fall back to english, if user locale doesn't exist.
I18n.fallbacks = true;

//Support translations
I18n.translations = {
    en, ar
}

const currentLocale = I18n.currentLocale();

//Is it RTL language?
export const isRTL = currentLocale.indexOf('en') === 0 || currentLocale.indexOf('ar') === 0;

ReactNative.I18nManager.allowRTL(isRTL);

export function strings(name, params = {}){
    return I18n.t(name, params);
};

