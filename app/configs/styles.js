import { StyleSheet } from 'react-native';
import colors from './colors';

export default StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: colors.background,
        alignItems: 'center',
    }, appName: {
        fontSize: 24,
        fontWeight: 'bold',
        fontFamily: 'roboto',
        color: colors.primaryText,
        position: 'relative',
        textAlign: 'center',
        marginTop: 15,
    }, version: {
        fontSize: 16,
        color: colors.primaryText,
        position: 'absolute',
        bottom: 10,
    }, center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});