import { createStackNavigator } from 'react-navigation';
import SplashScreen from '../screens/Splash';
import LandingScreen from '../screens/Landing';
import ReviewScreen from '../screens/Review';
import colors from '../configs/colors';

export const RouteStack = createStackNavigator(
    {
        Splash: {
            screen: SplashScreen,
            navigationOptions: ({ navigation }) => ({
                title: 'Splash',
                header: null,
            }),
        }, Landing: {
            screen: LandingScreen,
            navigationOptions: (navigation) => ({
                title: 'Landing',
                header: null,
            }),
        }, Review: {
            screen: ReviewScreen,
            navigationOptions: (navigator) => ({
                title: 'Review Colleague shop',
                // header: null,
                // headerMode: 'float',
                headerStyle: {
                    backgroundColor: colors.background,
                },
                headerTintColor: colors.primaryText,
                headerTitleStyle: {
                    fontWeight: 'normal',
                    fontSize: 22
                },
            })
        }
    },
    {
        //Default configs for all screen
        // headerMode: 'none',
        title: 'Waste',
        initialRouteName: 'Splash',
    });