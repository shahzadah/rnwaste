export default {
    background: '#00539f',
    statusBar: '#00539f',
    primaryText: '#FFFFFF',
    progressBar: '#008dc8',
    review_item_background: '#FFFFFF',
    grey_one: '#333333',
    grey_two: '#666666',
    grey_three: '#cccccc',
}