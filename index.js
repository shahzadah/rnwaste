import { AppRegistry } from 'react-native';
import { RouteStack } from './app/configs/routers';

AppRegistry.registerComponent('RNWaste', () => RouteStack);